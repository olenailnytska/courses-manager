const pipeDuration = (count) => {
	let hours = Math.trunc(count / 60);
	let minutes = count % 60;
	if (hours >= 0 && hours < 10) {
		hours = '0' + hours;
	}
	if (minutes >= 0 && minutes < 10) {
		minutes = '0' + minutes;
	}
	return hours + ':' + minutes;
};

export default pipeDuration;
