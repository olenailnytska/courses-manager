const services = {
	registerUser: (newUser) =>
		fetch('http://localhost:4000/register', {
			method: 'POST',
			body: JSON.stringify(newUser),
			headers: {
				'Content-Type': 'application/json',
			},
		}).then((response) => {
			if (!response.ok) throw new Error(response.status);
			else return response.json();
		}),
	loginUser: (user) =>
		fetch('http://localhost:4000/login', {
			method: 'POST',
			body: JSON.stringify(user),
			headers: {
				'Content-Type': 'application/json',
			},
		}).then((response) => {
			if (!response.ok) throw new Error(response.status);
			else return response.json();
		}),
	logoutUser: () =>
		fetch('http://localhost:4000/logout', {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: localStorage.token,
			},
		}).then((response) => {
			if (!response.ok) throw new Error(response.status);
			else return response;
		}),
	getCurrentUser: () =>
		fetch('http://localhost:4000/users/me', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: localStorage.token,
			},
		}).then((response) => {
			if (!response.ok) throw new Error(response.status);
			else return response.json();
		}),
	getCourses: () =>
		fetch('http://localhost:4000/courses/all', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		}).then((response) => {
			if (!response.ok) throw new Error(response.status);
			else return response.json();
		}),
	deleteCourse: (id) =>
		fetch(`http://localhost:4000/courses/${id}`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: localStorage.token,
			},
		}).then((response) => {
			if (!response.ok) throw new Error(response.status);
			else return response.json();
		}),
	updateCourse: (newCourse, id) =>
		fetch(`http://localhost:4000/courses/${id}`, {
			method: 'PUT',
			body: JSON.stringify(newCourse),
			headers: {
				'Content-Type': 'application/json',
				Authorization: localStorage.token,
			},
		}).then((response) => {
			console.log(response.result);
			if (!response.ok) throw new Error(response.status);
			else return response.json();
		}),
	addNewCourse: (newCourse) =>
		fetch(`http://localhost:4000/courses/add`, {
			method: 'POST',
			body: JSON.stringify(newCourse),
			headers: {
				'Content-Type': 'application/json',
				Authorization: localStorage.token,
			},
		}).then((response) => {
			if (!response.ok) throw new Error(response.status);
			else return response.json();
		}),
	getAuthors: () =>
		fetch('http://localhost:4000/authors/all', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		}).then((response) => {
			if (!response.ok) throw new Error(response.status);
			else return response.json();
		}),
	addNewAuthor: (newAuthor) =>
		fetch(`http://localhost:4000/authors/add`, {
			method: 'POST',
			body: JSON.stringify(newAuthor),
			headers: {
				'Content-Type': 'application/json',
				Authorization: localStorage.token,
			},
		}).then((response) => {
			if (!response.ok) throw new Error(response.status);
			else return response.json();
		}),
};

export default services;
