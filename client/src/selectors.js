export const selectCourses = (state) => state.courses;
export const selectAuthors = (state) => state.authors;
export const selectUserRole = (state) => state.user.role;
export const selectUserName = (state) => state.user.name;
