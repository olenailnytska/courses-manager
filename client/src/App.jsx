import { useEffect } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CourseForm from './components/CourseForm/CourseForm';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import CourseInfo from './components/CourseInfo/CourseInfo';
import { PrivateRouter } from './components/PrivateRouter/PrivateRouter';
import { AuthorizedRouter } from './components/AuhorizedRouter/AuthorizedRouter';
import { getCourses } from './store/courses/thunk';
import { getAuthors } from './store/authors/thunk';

import './App.css';

function App() {
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getCourses);
	}, []);

	useEffect(() => {
		dispatch(getAuthors);
	}, []);

	return (
		<div>
			<Header />
			<Routes>
				<Route
					path='/'
					element={
						localStorage.token ? (
							<Navigate to='/courses' />
						) : (
							<Navigate to='/login' />
						)
					}
				></Route>
				<Route element={<AuthorizedRouter />}>
					<Route path='/courses' element={<Courses />}></Route>
					<Route path='/courses/:courseId' element={<CourseInfo />}></Route>
					<Route element={<PrivateRouter />}>
						<Route path='/courses/add' element={<CourseForm />} />
						<Route path='/courses/update/:courseId' element={<CourseForm />} />
					</Route>
				</Route>
				<Route path='/registration' element={<Registration />}></Route>
				<Route path='/login' element={<Login />}></Route>
			</Routes>
		</div>
	);
}

export default App;
