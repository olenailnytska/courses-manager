import PropTypes from 'prop-types';

import './Button.css';

const Button = ({ onClick, buttonText, children, className }) => {
	return (
		<button className={`button ${className}`} onClick={onClick}>
			{buttonText || children}
		</button>
	);
};

Button.propTypes = {
	onClick: PropTypes.func,
	buttonText: PropTypes.string,
	children: PropTypes.element,
	className: PropTypes.string,
};

export default Button;
