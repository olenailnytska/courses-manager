import PropTypes from 'prop-types';

import './Input.css';

const Input = ({
	className,
	labelText,
	placeholderText,
	onChange,
	value,
	type,
}) => {
	return (
		<div className={className}>
			<label htmlFor='input'>{labelText}</label>
			<input
				type={type}
				placeholder={placeholderText}
				onChange={onChange}
				value={value}
			></input>
		</div>
	);
};

Input.propTypes = {
	className: PropTypes.string,
	labelText: PropTypes.string,
	placeholderText: PropTypes.string,
	onChange: PropTypes.func,
	value: PropTypes.string,
	type: PropTypes.string,
};

export default Input;
