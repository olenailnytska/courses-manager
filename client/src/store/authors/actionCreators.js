import { GET_ALL_AUTHORS, CREATE_AUTHOR } from './actionTypes';

export function getAllAuthors(authors) {
	return { type: GET_ALL_AUTHORS, payload: authors };
}

export function createAuthor(author) {
	return { type: CREATE_AUTHOR, payload: author };
}
