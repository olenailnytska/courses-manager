import services from '../../services';
import { GET_ALL_AUTHORS, CREATE_AUTHOR } from './actionTypes';

export async function getAuthors(dispatch) {
	const response = await services.getAuthors();
	console.log(response.successful);
	if (response.successful) {
		dispatch({ type: GET_ALL_AUTHORS, payload: response.result });
	}
}

export function createAuthor(newAuthor) {
	return async function logoutUserThunk(dispatch) {
		const response = await services.addNewAuthor(newAuthor);
		if (response.successful) {
			dispatch({ type: CREATE_AUTHOR, payload: response.result });
		}
	};
}
