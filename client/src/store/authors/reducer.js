import { GET_ALL_AUTHORS, CREATE_AUTHOR } from './actionTypes';

const initialState = [];

export default function authorsReducer(state = initialState, action) {
	switch (action.type) {
		case GET_ALL_AUTHORS: {
			return [...action.payload];
		}
		case CREATE_AUTHOR: {
			return [...state, action.payload];
		}
		default:
			return state;
	}
}
