import {
	GET_ALL_COURSES,
	DELETE_COURSE,
	CREATE_COURSE,
	UPDATE_COURSE,
} from './actionTypes';

export function getAllCourses(courses) {
	return { type: GET_ALL_COURSES, payload: courses };
}

export function deleteCourse(id) {
	return { type: DELETE_COURSE, payload: id };
}

export function createCourse(course) {
	return { type: CREATE_COURSE, payload: course };
}

export function updateCourse(course) {
	return { type: UPDATE_COURSE, payload: course };
}
