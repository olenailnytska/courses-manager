import {
	GET_ALL_COURSES,
	DELETE_COURSE,
	CREATE_COURSE,
	UPDATE_COURSE,
} from './actionTypes';

const initialState = [];

export default function coursesReducer(state = initialState, action) {
	switch (action.type) {
		case GET_ALL_COURSES: {
			return [...action.payload];
		}
		case DELETE_COURSE: {
			return [
				...state.filter((course) => {
					return course.id !== action.payload;
				}),
			];
		}
		case CREATE_COURSE: {
			return [...state, action.payload];
		}
		case UPDATE_COURSE: {
			let courses = [...state];
			const index = state.map((course) => course.id).indexOf(action.payload.id);
			courses[index] = action.payload;
			return [...courses];
		}
		default:
			return state;
	}
}
