import services from '../../services';
import {
	GET_ALL_COURSES,
	CREATE_COURSE,
	UPDATE_COURSE,
	DELETE_COURSE,
} from './actionTypes';

export async function getCourses(dispatch) {
	const response = await services.getCourses();
	console.log(response);
	if (response.successful) {
		dispatch({
			type: GET_ALL_COURSES,
			payload: response.result,
		});
	}
	return response.result;
}

export function addNewCourse(newCourse) {
	return async function deleteCourseThunk(dispatch) {
		const response = await services.addNewCourse(newCourse);
		if (response.successful) {
			dispatch({ type: CREATE_COURSE, payload: response.result });
		}
	};
}

export function updateCourse(newCourse, id) {
	return async function deleteCourseThunk(dispatch) {
		const response = await services.updateCourse(newCourse, id);
		if (response.successful) {
			dispatch({ type: UPDATE_COURSE, payload: response.result });
		}
	};
}

export function deleteCourse(id) {
	return async function deleteCourseThunk(dispatch) {
		const response = await services.deleteCourse(id);
		if (response.successful) {
			dispatch({ type: DELETE_COURSE, payload: id });
		}
	};
}
