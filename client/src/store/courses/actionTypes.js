export const GET_ALL_COURSES = 'courses/getAllCourses';
export const DELETE_COURSE = 'courses/deleteCourse';
export const CREATE_COURSE = 'authors/createCourse';
export const UPDATE_COURSE = 'authors/updateCourse';
