import services from '../../services';
import { LOGIN_USER, LOGOUT_USER, GET_USER } from './actionTypes';

export function loginUser(user) {
	return async function loginUserThunk(dispatch) {
		const response = await services.loginUser(user);
		if (response.successful) {
			dispatch({ type: LOGIN_USER, payload: response.result });
		}
		return response.result;
	};
}

export async function logoutUser(dispatch) {
	const response = await services.logoutUser();
	if (response.ok) {
		dispatch({ type: LOGOUT_USER });
	}
}

export async function getCurrentUser(dispatch) {
	console.log(localStorage.token);
	const response = await services.getCurrentUser();
	console.log(response);
	if (response.successful) {
		dispatch({
			type: GET_USER,
			payload: Object.assign(response.result, { token: localStorage.token }),
		});
	}
	return response.result;
}
