import { LOGIN_USER, LOGOUT_USER, GET_USER } from './actionTypes';

const initialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
	role: '',
};

export default function userReducer(state = initialState, action) {
	switch (action.type) {
		case LOGIN_USER: {
			return {
				isAuth: true,
				token: action.payload,
			};
		}
		case LOGOUT_USER: {
			return {
				isAuth: false,
				name: '',
				email: '',
				token: '',
				role: '',
			};
		}
		case GET_USER: {
			return {
				...state,
				isAuth: true,
				name: action.payload.name,
				email: action.payload.email,
				role: action.payload.role,
				token: action.payload.token,
			};
		}
		default:
			return state;
	}
}
