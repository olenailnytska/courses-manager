export const LOGIN_USER = 'users/loginUser';
export const LOGOUT_USER = 'users/logoutUser';
export const GET_USER = 'users/getUser';
