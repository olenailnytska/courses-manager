import { Navigate, Outlet } from 'react-router-dom';

export const AuthorizedRouter = () => {
	if (!localStorage.token) {
		return <Navigate to='/login' />;
	}
	return <Outlet />;
};
