import { Link, useNavigate } from 'react-router-dom';
import { useState } from 'react';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import services from '../../services';

import {
	LABEL_TEXT_NAME,
	LABEL_TEXT_EMAIL,
	LABEL_TEXT_PASSWORD,
	PLACEHOLDER_TEXT_NAME,
	PLACEHOLDER_TEXT_EMAIL,
	PLACEHOLDER_TEXT_PASSWORD,
	BUTTON_TEXT_REGISTRATION,
} from '../../constants';

import './Registration.css';

const Registration = () => {
	const navigate = useNavigate();
	const [newUser, SetNewUser] = useState({
		name: '',
		email: '',
		password: '',
	});
	const [errors, setErrors] = useState([]);

	const errorsList = errors.map((error) => <p className='error'>{error}</p>);

	const onChangeName = (e) => {
		const value = e.target.value;
		SetNewUser((prevState) => ({
			...prevState,
			name: value,
		}));
	};

	const onChangeEmail = (e) => {
		const value = e.target.value;
		SetNewUser((prevState) => ({
			...prevState,
			email: value,
		}));
	};

	const onChangePassword = (e) => {
		const value = e.target.value;
		SetNewUser((prevState) => ({
			...prevState,
			password: value,
		}));
	};

	const onSubmit = (e) => {
		e.preventDefault();
		services.registerUser(newUser).then((data) => {
			if (data.successful) {
				navigate('/login');
			} else {
				setErrors(data.errors);
			}
		});
	};

	return (
		<div className='container courses registration'>
			<h2>Registration</h2>
			<form onSubmit={onSubmit}>
				<Input
					labelText={LABEL_TEXT_NAME}
					type='text'
					placeholderText={PLACEHOLDER_TEXT_NAME}
					onChange={onChangeName}
				/>
				<Input
					labelText={LABEL_TEXT_EMAIL}
					type='email'
					placeholderText={PLACEHOLDER_TEXT_EMAIL}
					onChange={onChangeEmail}
				/>
				<Input
					labelText={LABEL_TEXT_PASSWORD}
					type='password'
					placeholderText={PLACEHOLDER_TEXT_PASSWORD}
					onChange={onChangePassword}
				/>
				<p>{errorsList}</p>
				<div className='button-wrapper'>
					<Button buttonText={BUTTON_TEXT_REGISTRATION} />
				</div>
			</form>
			<p>
				If you have an account you can <Link to='/login'>Login</Link>
			</p>
		</div>
	);
};

export default Registration;
