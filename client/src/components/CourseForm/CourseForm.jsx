import { useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import { selectCourses, selectAuthors } from '../../selectors';
import { createAuthor } from '../../store/authors/thunk';
import {
	addNewCourse,
	updateCourse,
	getCourses,
} from '../../store/courses/thunk';

import dateGenerator from '../../helpers/dateGenerator';
import pipeDuration from '../../helpers/pipeDuration';

import {
	BUTTON_TEXT_ADD_AUTHOR,
	BUTTON_TEXT_DELETE_AUTHOR,
	BUTTON_TEXT_CREATE_AUTHOR,
	BUTTON_TEXT_CREATE_COURSE,
	BUTTON_TEXT_UPDATE_COURSE,
	PLACEHOLDER_TEXT_TITLE,
	PLACEHOLDER_TEXT_AUTHOR_NAME,
	PLACEHOLDER_TEXT_DURATION,
	LABEL_TEXT_TITLE,
	LABEL_TEXT_AUTHOR_NAME,
	LABEL_TEXT_DURATION,
} from '../../constants';

import './CourseForm.css';

const CreateCourse = () => {
	let { courseId } = useParams();
	courseId = courseId && courseId.slice(1);

	let courses = useSelector(selectCourses);
	const dispatch = useDispatch();

	let titleDefault = '';
	let descriptionDefault = '';
	let durationDefault = '';
	let authorsDefault = [];
	let idDefault;
	let creationDateDefault;

	let authorsList = useSelector(selectAuthors);
	if (courses.length > 0 && courseId) {
		const index = courses.map((author) => author.id).indexOf(courseId);
		const course = courses[index];
		titleDefault = course.title;
		descriptionDefault = course.description;
		durationDefault = course.duration;
		authorsDefault = course.authors;
		idDefault = course.id;
		creationDateDefault = course.creationDate;
	}
	const [title, setTitle] = useState(titleDefault);
	const [description, setDescription] = useState(descriptionDefault);
	const [authorName, setAuthorName] = useState('');
	const [duration, setDuration] = useState(durationDefault);
	const [courseAuthorsList, setCourseAuthorsList] = useState(authorsDefault);

	if (courses.length === 0) {
		dispatch(getCourses).then((courses) => {
			const index = courses.map((author) => author.id).indexOf(courseId);
			const course = courses[index];
			setTitle(course.title);
			setDescription(course.description);
			setDuration(course.duration);
			setCourseAuthorsList(course.authors);
			idDefault = course.id;
			creationDateDefault = course.creationDate;
		});
	}

	const onChangeTitle = (e) => {
		const value = e.target.value;
		setTitle(value);
	};

	const onChangeDescription = (e) => {
		const value = e.target.value;
		setDescription(value);
	};

	const onChangeAuthorName = (e) => {
		const value = e.target.value;
		setAuthorName(value);
	};

	const onChangeDuration = (e) => {
		const value = e.target.value;
		if (value === '' || /^[0-9]*$/.test(value)) {
			setDuration(value);
		}
	};

	const onCreateAuthor = () => {
		const newAuthor = {
			name: authorName,
		};

		if (authorName.length >= 2) {
			dispatch(createAuthor(newAuthor));
		}
	};

	const onAddAuthor = (id) => {
		const index = authorsList.map((author) => author.id).indexOf(id);
		setCourseAuthorsList([...courseAuthorsList, authorsList[index].id]);
		authorsList = authorsList.filter((author) => author.id !== id);
	};

	const onDeleteAuthor = (id) => {
		setCourseAuthorsList(courseAuthorsList.filter((el) => el !== id));
	};

	console.log(localStorage);

	const authors = authorsList
		.filter((author) => !courseAuthorsList.includes(author.id))
		.map((author) => {
			return (
				<div className='author' key={author.id}>
					<p>{author.name}</p>
					<Button
						buttonText={BUTTON_TEXT_ADD_AUTHOR}
						onClick={() => onAddAuthor(author.id)}
					/>
				</div>
			);
		});

	const courseAuthors = authorsList
		.filter((author) => courseAuthorsList.includes(author.id))
		.map((author) => {
			return (
				<div className='author' key={author.id}>
					<p>{author.name}</p>
					<Button
						buttonText={BUTTON_TEXT_DELETE_AUTHOR}
						onClick={() => onDeleteAuthor(author.id)}
					/>
				</div>
			);
		});

	const navigate = useNavigate();

	const onCreateCourse = () => {
		const authorsId = courseAuthorsList.map((author) => author);
		if (
			title === '' ||
			description.length < 2 ||
			duration === '' ||
			duration === '0' ||
			authorsId.length === 0
		) {
			alert('Please, fill in all fields');
			return;
		}
		const newCourse = {
			title,
			description,
			creationDate: creationDateDefault || dateGenerator(),
			duration: +duration,
			authors: authorsId,
		};
		if (courseId !== undefined) {
			dispatch(updateCourse(newCourse, idDefault));
		} else {
			dispatch(addNewCourse(newCourse));
		}
		navigate('/courses');
	};

	return (
		<div className='container courses'>
			<div className='container'>
				<div className='flex-container'>
					<Input
						labelText={LABEL_TEXT_TITLE}
						placeholderText={PLACEHOLDER_TEXT_TITLE}
						value={title}
						onChange={onChangeTitle}
						className='title'
					/>
					<Button
						buttonText={
							courseId ? BUTTON_TEXT_UPDATE_COURSE : BUTTON_TEXT_CREATE_COURSE
						}
						onClick={onCreateCourse}
					></Button>
				</div>
				<label htmlFor='description' className='description'>
					Description
				</label>
				<textarea
					placeholder='Enter description'
					className='description'
					id='description'
					value={description}
					onChange={onChangeDescription}
				/>
				<div className='additional-information'>
					<div className='flex-container'>
						<div className='column1'>
							<h3>Add author</h3>
							<Input
								labelText={LABEL_TEXT_AUTHOR_NAME}
								placeholderText={PLACEHOLDER_TEXT_AUTHOR_NAME}
								value={authorName}
								onChange={onChangeAuthorName}
							/>
							<Button
								buttonText={BUTTON_TEXT_CREATE_AUTHOR}
								onClick={onCreateAuthor}
							/>
							<h3>Duration</h3>
							<Input
								labelText={LABEL_TEXT_DURATION}
								placeholderText={PLACEHOLDER_TEXT_DURATION}
								value={duration.toString()}
								onChange={onChangeDuration}
							/>
							<p className='duration'>
								Duration: <span>{pipeDuration(duration)}</span> hours
							</p>
						</div>
						<div className='column2'>
							<h3>Authors</h3>
							{authors}
							<h3>Course authors</h3>
							{courseAuthorsList.length === 0 ? (
								<p>Authors list is empty</p>
							) : (
								courseAuthors
							)}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CreateCourse;
