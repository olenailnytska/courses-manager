import logo from './logo.png';

import './logo.css';

const Logo = () => <img alt='logo' src={logo} className='logo' />;

export default Logo;
