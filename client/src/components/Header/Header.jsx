import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import { BUTTON_TEXT_LOGOUT } from '../../constants';
import { logoutUser } from '../../store/user/thunk';
import { selectUserName } from '../../selectors';

import './Header.css';

const Header = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	let userName = useSelector(selectUserName);

	const onClick = () => {
		dispatch(logoutUser).then(() => {
			localStorage.clear();
			navigate('/login');
		});
	};

	console.log(localStorage);

	return (
		<div className='header container'>
			<div className='border header-content'>
				<Logo />
				<div className='user'>
					{localStorage.token && (
						<>
							<p>{userName}</p>
							<Button buttonText={BUTTON_TEXT_LOGOUT} onClick={onClick} />
						</>
					)}
				</div>
			</div>
		</div>
	);
};

export default Header;
