import PropTypes from 'prop-types';

import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';
import {
	PLACEHOLDER_TEXT_SEARCH,
	BUTTON_TEXT_SEARCH,
} from '../../../../constants';

import './SearchBar.css';

const SearchBar = ({ searchParam, setSearchParam, setSearchClicked }) => {
	const onChange = (e) => {
		const value = e.target.value;
		setSearchParam(value);
		console.log(searchParam);
		if (value === '') {
			setSearchClicked(false);
		}
	};

	const onClick = () => {
		setSearchClicked(true);
	};

	return (
		<div className='search-bar column'>
			<Input placeholderText={PLACEHOLDER_TEXT_SEARCH} onChange={onChange} />
			<Button buttonText={BUTTON_TEXT_SEARCH} onClick={onClick} />
		</div>
	);
};

SearchBar.propTypes = {
	searchParam: PropTypes.string,
	setSearchParam: PropTypes.func,
	setSearchClicked: PropTypes.func,
};

export default SearchBar;
