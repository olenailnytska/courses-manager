import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen, faTrash } from '@fortawesome/free-solid-svg-icons';
import { useDispatch, useSelector } from 'react-redux';

import Button from '../../../../common/Button/Button';
import pipeDuration from '../../../../helpers/pipeDuration';
import { BUTTON_TEXT_SHOW_COURSE } from '../../../../constants';
import { selectUserRole } from '../../../../selectors';
import { deleteCourse } from '../../../../store/courses/thunk';

import './CourseCard.css';

const CourseCard = ({
	id,
	title,
	desription,
	authors,
	duration,
	creationDate,
}) => {
	const dispatch = useDispatch();
	let userRole = useSelector(selectUserRole);

	const onDelete = () => {
		dispatch(deleteCourse(id));
	};

	return (
		<div className='border course'>
			<div className='description block'>
				<h2>{title}</h2>
				<div>{desription}</div>
			</div>
			<div className='information block'>
				<div>
					<span>Authors: </span>
					{authors.join(', ')}
				</div>
				<div>
					<span>Duration: </span>
					{pipeDuration(duration) + ' hours'}
				</div>
				<div>
					<span>Created: </span>
					{creationDate.replaceAll('/', '.')}
				</div>
				<div className='button-wrapper'>
					<Link to={`./:${id}`}>
						<Button buttonText={BUTTON_TEXT_SHOW_COURSE} />
					</Link>
					{userRole === 'admin' && (
						<>
							<Link to={`./update/:${id}`}>
								<Button className='icon-button'>
									<FontAwesomeIcon className='icon' icon={faPen} />
								</Button>
							</Link>
							<Button className='icon-button' onClick={onDelete}>
								<FontAwesomeIcon className='icon' icon={faTrash} />
							</Button>
						</>
					)}
				</div>
			</div>
			<br></br>
		</div>
	);
};

CourseCard.propTypes = {
	id: PropTypes.string,
	title: PropTypes.string,
	desription: PropTypes.string,
	authors: PropTypes.array,
	duration: PropTypes.number,
	creationDate: PropTypes.string,
};

export default CourseCard;
