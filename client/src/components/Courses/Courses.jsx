import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import { selectCourses, selectAuthors, selectUserRole } from '../../selectors';
import { getCurrentUser } from '../../store/user/thunk';
import { BUTTON_TEXT_ADD_NEW_COURSE } from '../../constants';

const Courses = () => {
	const [searchParam, setSearchParam] = useState('');
	const [searchClicked, setSearchClicked] = useState(false);
	let userRole = useSelector(selectUserRole);
	let coursesList = useSelector(selectCourses);
	let authorsList = useSelector(selectAuthors);
	const dispatch = useDispatch();
	console.log(coursesList);
	console.log(authorsList);

	useEffect(() => {
		dispatch(getCurrentUser).then((user) => {
			localStorage.setItem('role', user.role);
		});
	}, []);

	const modifyString = (param) => {
		return param.toLowerCase();
	};

	const search = (items) => {
		return items.filter((course) => {
			return !searchClicked ||
				modifyString(course.title).includes(modifyString(searchParam)) ||
				modifyString(course.id).includes(modifyString(searchParam))
				? course
				: 0;
		});
	};

	const courses = search(coursesList).map((course) => {
		const authors = course.authors.map((author) => {
			return (
				authorsList.find((item) => item.id === author) &&
				authorsList.find((item) => item.id === author).name
			);
		});

		return (
			<CourseCard
				key={course.id}
				id={course.id}
				title={course.title}
				desription={course.description}
				authors={authors}
				duration={course.duration}
				creationDate={course.creationDate}
			/>
		);
	});

	return (
		<div className='container courses'>
			<div className='container search-bar'>
				<SearchBar
					searchParam={searchParam}
					setSearchParam={setSearchParam}
					searchClicked={searchClicked}
					setSearchClicked={setSearchClicked}
				/>
				{userRole === 'admin' && (
					<Link to='./add'>
						<Button buttonText={BUTTON_TEXT_ADD_NEW_COURSE} />
					</Link>
				)}
			</div>
			{courses}
		</div>
	);
};

export default Courses;
