import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { useDispatch } from 'react-redux';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';

import {
	LABEL_TEXT_EMAIL,
	LABEL_TEXT_PASSWORD,
	PLACEHOLDER_TEXT_EMAIL,
	PLACEHOLDER_TEXT_PASSWORD,
	BUTTON_TEXT_LOGIN,
} from '../../constants';
import { loginUser } from '../../store/user/thunk';

const Login = () => {
	const navigate = useNavigate();
	const [newUser, SetNewUser] = useState({
		email: '',
		password: '',
	});
	const error = useState('');
	const dispatch = useDispatch();

	const onSubmit = (e) => {
		e.preventDefault();
		dispatch(loginUser(newUser)).then((token) => {
			localStorage.setItem('token', token);
			navigate('/courses');
		});
	};

	const onChangeEmail = (e) => {
		const value = e.target.value;
		SetNewUser((prevState) => ({
			...prevState,
			email: value,
		}));
	};

	const onChangePassword = (e) => {
		const value = e.target.value;
		SetNewUser((prevState) => ({
			...prevState,
			password: value,
		}));
	};

	return (
		<div className='container courses registration'>
			<h2>Login</h2>
			<form onSubmit={onSubmit}>
				<Input
					labelText={LABEL_TEXT_EMAIL}
					type='email'
					placeholderText={PLACEHOLDER_TEXT_EMAIL}
					onChange={onChangeEmail}
				/>
				<Input
					labelText={LABEL_TEXT_PASSWORD}
					type='password'
					placeholderText={PLACEHOLDER_TEXT_PASSWORD}
					onChange={onChangePassword}
				/>
				<p className='error'>{error}</p>
				<div className='button-wrapper'>
					<Button buttonText={BUTTON_TEXT_LOGIN} />
				</div>
			</form>
			<p>
				If you not have an account you can{' '}
				<Link to='/registration'>Registration</Link>
			</p>
		</div>
	);
};

export default Login;
