import { Link, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { selectCourses, selectAuthors } from '../../selectors';
import { getCourses } from '../../store/courses/thunk';
import { getAuthors } from '../../store/authors/thunk';
import pipeDuration from '../../helpers/pipeDuration';

const CourseInfo = () => {
	let { courseId } = useParams();
	courseId = courseId.slice(1);
	const dispatch = useDispatch();

	let courses = useSelector(selectCourses);
	courses.length === 0 && dispatch(getCourses);
	let index = courses.map((author) => author.id).indexOf(courseId);
	let course = courses[index];

	let authors = useSelector(selectAuthors);
	authors.length === 0 && dispatch(getAuthors);

	return (
		courses.length > 0 && (
			<div className='container courses block'>
				<p>
					<Link to='/courses'>
						<span>&lt;</span> Back to courses
					</Link>
				</p>
				<h2>{course.title}</h2>
				<div className='content'>
					<div className='description'>
						<div>{course.description}</div>
					</div>
					<div className='information'>
						<div>
							<span>ID: </span>
							{course.id}
						</div>
						<div>
							<span>Duration: </span>
							{pipeDuration(course.duration) + ' hours'}
						</div>
						<div>
							<span>Created: </span>
							{course.creationDate.replaceAll('/', '.')}
						</div>
						<div>
							<span>Authors: </span>
							{authors.length > 0 &&
								course.authors.map((author, index) => {
									return (
										<p key={index}>
											{authors.find((item) => item.id === author).name}
										</p>
									);
								})}
						</div>
					</div>
				</div>
			</div>
		)
	);
};

export default CourseInfo;
