import { Navigate, Outlet } from 'react-router-dom';
import PropTypes from 'prop-types';

export const PrivateRouter = () => {
	let role = localStorage.role;
	if (role !== 'admin') {
		return <Navigate to='/courses' />;
	}
	return <Outlet />;
};

PrivateRouter.propTypes = {
	children: PropTypes.element,
};
